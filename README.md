# paperize

A minimal windows automatic wallpaper change app!

![Alt text](https://www.paperize.co/images/app.jpg "Optional title")

### Prerequisites



* [NodeJs](https://nodejs.org/en/)


### Installing

Clone the project and install the dependencies using the command

```
npm install
```

## Running paperize

```
npm start
```

## Create windows executable and installer

Execute the command below. Distributables will be located inside the dist folder at the root folder.

```
npm run dist
```

## Built With

* [Electron](https://www.electronjs.org/) 

## License

This project is licensed under the MIT License

